package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"

	"github.com/google/uuid"
	"gitlab.com/adred/go-graphql-gateway-demo/user/graph/generated"
	"gitlab.com/adred/go-graphql-gateway-demo/user/graph/model"
)

func (r *mutationResolver) CreateUser(ctx context.Context, input model.CreateUser) (*model.User, error) {
	u, err := r.UserService.CreateUser(&model.User{
		ID:       uuid.NewString(),
		Username: input.Username,
	})
	if err != nil {
		return &model.User{}, err
	}
	return u, nil
}

func (r *productResolver) Users(ctx context.Context, obj *model.Product) ([]*model.User, error) {
	u, err := r.UsersByProductService.GetUsersByProduct(obj.Upc)
	if err != nil {
		return []*model.User{}, err
	}

	var users []*model.User
	for _, val := range u {
		user, err := r.UserService.GetUser(val.UserID)
		if err != nil {
			return []*model.User{}, err
		}
		users = append(users, user)
	}
	return users, nil
}

func (r *queryResolver) GetUser(ctx context.Context, input model.GetUser) (*model.User, error) {
	u, err := r.UserService.GetUser(input.ID)
	fmt.Println("input.ID", input.ID)
	fmt.Println("err", err)
	if err != nil {
		return &model.User{}, err
	}
	return u, nil
}

func (r *queryResolver) GetAllUsers(ctx context.Context) ([]*model.User, error) {
	u, err := r.UserService.GetAllUsers()
	if err != nil {
		return []*model.User{}, err
	}
	return u, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Product returns generated.ProductResolver implementation.
func (r *Resolver) Product() generated.ProductResolver { return &productResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type productResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
