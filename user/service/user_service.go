package service

import (
	"fmt"

	"gitlab.com/adred/go-graphql-gateway-demo/user/graph/model"
	"gitlab.com/adred/go-graphql-gateway-demo/user/repository"
)

type UserServiceInterface interface {
	CreateUser(*model.User) (*model.User, error)
	GetUser(id string) (*model.User, error)
	GetAllUsers() ([]*model.User, error)
	UpdateUser(user *model.User) (*model.User, error)
	DeleteUser(id string) error
}

type UserService struct {
	UserRepo *repository.UserRepository
}

func NewUserService(tr *repository.UserRepository) *UserService {
	return &UserService{UserRepo: tr}
}

// UserService implements the model.UserInterface interface
var _ UserServiceInterface = &UserService{}

func (us *UserService) CreateUser(user *model.User) (*model.User, error) {
	fmt.Println(user.ID)
	user, err := us.UserRepo.CreateUser(user)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (us *UserService) GetUser(id string) (*model.User, error) {
	user, err := us.UserRepo.GetUser(id)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (us *UserService) GetAllUsers() ([]*model.User, error) {
	users, err := us.UserRepo.GetAllUsers()
	if err != nil {
		return nil, err
	}
	return users, nil
}

func (us *UserService) UpdateUser(user *model.User) (*model.User, error) {
	user, err := us.UserRepo.UpdateUser(user)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (us *UserService) DeleteUser(id string) error {
	err := us.UserRepo.DeleteUser(id)
	if err != nil {
		return err
	}

	return nil
}
