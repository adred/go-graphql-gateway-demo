package repository

import (
	"github.com/gocql/gocql"

	"gitlab.com/adred/go-graphql-gateway-demo/user/graph/model"
)

var (
	userTable = "user"

	createUserQuery  = "INSERT INTO " + userTable + " (id, username) VALUES(?, ?)"
	getUserQuery     = "SELECT * FROM " + userTable + " WHERE id=? LIMIT 1"
	getAllUsersQuery = "SELECT * FROM " + userTable + ""
	updateUserQuery  = "UPDATE " + userTable + " SET username = ? WHERE id = ?"
	deleteUserQuery  = "DELETE FROM " + userTable + " WHERE id = ?"
)

type UserRepositoryInterface interface {
	CreateUser(*model.User) (*model.User, error)
	GetUser(id string) (*model.User, error)
	GetAllUsers() ([]*model.User, error)
	UpdateUser(user *model.User) (*model.User, error)
	DeleteUser(id string) error
}
type UserRepository struct {
	session *gocql.Session
}

func NewUserRepository(session *gocql.Session) *UserRepository {
	return &UserRepository{
		session,
	}
}

var _ UserRepositoryInterface = &UserRepository{}

func (ur *UserRepository) CreateUser(user *model.User) (*model.User, error) {
	if err := ur.session.Query(createUserQuery, user.ID,
		user.Username).Exec(); err != nil {
		return &model.User{}, err
	}
	return user, nil
}

func (ur *UserRepository) GetUser(id string) (*model.User, error) {
	var _id gocql.UUID
	var username string

	err := ur.session.Query(getUserQuery,
		id).Scan(&_id, &username)

	if err != nil {
		return &model.User{}, err
	}

	return &model.User{
		ID:       _id.String(),
		Username: username,
	}, nil
}

func (ur *UserRepository) GetAllUsers() ([]*model.User, error) {
	var users []*model.User
	t := map[string]interface{}{}

	iter := ur.session.Query(getAllUsersQuery).Iter()
	for iter.MapScan(t) {
		users = append(users, &model.User{
			ID:       t["id"].(gocql.UUID).String(),
			Username: t["username"].(string),
		})
		t = map[string]interface{}{}
	}

	return users, nil
}

func (ur *UserRepository) UpdateUser(user *model.User) (*model.User, error) {
	if err := ur.session.Query(updateUserQuery, user.Username, user.ID).Exec(); err != nil {
		return &model.User{}, err
	}

	return user, nil
}

func (ur *UserRepository) DeleteUser(id string) error {
	if err := ur.session.Query(deleteUserQuery, id).Exec(); err != nil {
		return err
	}

	return nil
}
