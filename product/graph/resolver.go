package graph

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

import (
	"gitlab.com/adred/go-graphql-gateway-demo/product/service"
)

type Resolver struct {
	ProductService        *service.ProductService
	ProductsByUserService *service.ProductsByUserService
}
