package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"github.com/google/uuid"
	"gitlab.com/adred/go-graphql-gateway-demo/product/graph/generated"
	"gitlab.com/adred/go-graphql-gateway-demo/product/graph/model"
)

func (r *mutationResolver) CreateProduct(ctx context.Context, input model.CreateProduct) (*model.Product, error) {
	p, err := r.ProductService.CreateProduct(&model.Product{
		Upc:     uuid.NewString(),
		Name:    input.Name,
		Price:   input.Price,
		InStock: input.InStock,
	})
	if err != nil {
		return &model.Product{}, err
	}
	return p, nil
}

func (r *queryResolver) GetAllProducts(ctx context.Context) ([]*model.Product, error) {
	p, err := r.ProductService.GetAllProducts()
	if err != nil {
		return []*model.Product{}, err
	}
	return p, nil
}

func (r *userResolver) ID(ctx context.Context, obj *model.User) (string, error) {
	return obj.ID, nil
}

func (r *userResolver) Products(ctx context.Context, obj *model.User) ([]*model.Product, error) {
	p, err := r.ProductsByUserService.GetProductsByUser(obj.ID)
	if err != nil {
		return []*model.Product{}, err
	}

	var products []*model.Product
	for _, val := range p {
		product, err := r.ProductService.GetProduct(val.UPC)
		if err != nil {
			return []*model.Product{}, err
		}
		products = append(products, product)
	}
	return products, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

// User returns generated.UserResolver implementation.
func (r *Resolver) User() generated.UserResolver { return &userResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
type userResolver struct{ *Resolver }
