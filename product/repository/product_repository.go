package repository

import (
	"github.com/gocql/gocql"

	"gitlab.com/adred/go-graphql-gateway-demo/product/graph/model"
)

var (
	productTable = "product"

	createProductQuery  = "INSERT INTO " + productTable + " (upc, in_stock, name, price) VALUES(?, ?, ?, ?)"
	getProductQuery     = "SELECT * FROM " + productTable + " WHERE upc=? LIMIT 1"
	getAllProductsQuery = "SELECT * FROM " + productTable + ""
	updateProductQuery  = "UPDATE " + productTable + " SET in_stock = ?, name = ?, price = ? WHERE upc = ?"
	deleteProductQuery  = "DELETE FROM " + productTable + " WHERE upc = ?"
)

type ProductRepositoryInterface interface {
	CreateProduct(*model.Product) (*model.Product, error)
	GetProduct(upc string) (*model.Product, error)
	GetAllProducts() ([]*model.Product, error)
	UpdateProduct(product *model.Product) (*model.Product, error)
	DeleteProduct(upc string) error
}
type ProductRepository struct {
	session *gocql.Session
}

func NewProductRepository(session *gocql.Session) *ProductRepository {
	return &ProductRepository{
		session,
	}
}

var _ ProductRepositoryInterface = &ProductRepository{}

func (ur *ProductRepository) CreateProduct(product *model.Product) (*model.Product, error) {
	if err := ur.session.Query(createProductQuery, product.Upc, product.InStock, product.Name,
		product.Price).Exec(); err != nil {
		return &model.Product{}, err
	}
	return product, nil
}

func (ur *ProductRepository) GetProduct(upc string) (*model.Product, error) {
	var _upc gocql.UUID
	var name string
	var inStock int
	var price int

	err := ur.session.Query(getProductQuery,
		upc).Scan(&_upc, &inStock, &name, &price)

	if err != nil {
		return &model.Product{}, err
	}

	return &model.Product{
		Upc:     _upc.String(),
		InStock: inStock,
		Name:    name,
		Price:   price,
	}, nil
}

func (ur *ProductRepository) GetAllProducts() ([]*model.Product, error) {
	var products []*model.Product
	t := map[string]interface{}{}

	iter := ur.session.Query(getAllProductsQuery).Iter()
	for iter.MapScan(t) {
		products = append(products, &model.Product{
			Upc:     t["upc"].(gocql.UUID).String(),
			InStock: t["in_stock"].(int),
			Name:    t["name"].(string),
			Price:   t["price"].(int),
		})
		t = map[string]interface{}{}
	}

	return products, nil
}

func (ur *ProductRepository) UpdateProduct(product *model.Product) (*model.Product, error) {
	if err := ur.session.Query(updateProductQuery, product.InStock, product.Name, product.Price, product.Upc).Exec(); err != nil {
		return &model.Product{}, err
	}

	return product, nil
}

func (ur *ProductRepository) DeleteProduct(upc string) error {
	if err := ur.session.Query(deleteProductQuery, upc).Exec(); err != nil {
		return err
	}

	return nil
}
