package service

import (
	"gitlab.com/adred/go-graphql-gateway-demo/product/graph/model"
	"gitlab.com/adred/go-graphql-gateway-demo/product/repository"
)

type ProductServiceInterface interface {
	CreateProduct(*model.Product) (*model.Product, error)
	GetProduct(upc string) (*model.Product, error)
	GetAllProducts() ([]*model.Product, error)
	UpdateProduct(product *model.Product) (*model.Product, error)
	DeleteProduct(upc string) error
}

type ProductService struct {
	ProductRepo *repository.ProductRepository
}

func NewProductService(tr *repository.ProductRepository) *ProductService {
	return &ProductService{ProductRepo: tr}
}

// ProductService implements the model.ProductInterface interface
var _ ProductServiceInterface = &ProductService{}

func (ps *ProductService) CreateProduct(product *model.Product) (*model.Product, error) {
	product, err := ps.ProductRepo.CreateProduct(product)
	if err != nil {
		return nil, err
	}

	return product, nil
}

func (ps *ProductService) GetProduct(upc string) (*model.Product, error) {
	product, err := ps.ProductRepo.GetProduct(upc)
	if err != nil {
		return nil, err
	}
	return product, nil
}

func (ps *ProductService) GetAllProducts() ([]*model.Product, error) {
	products, err := ps.ProductRepo.GetAllProducts()
	if err != nil {
		return nil, err
	}
	return products, nil
}

func (ps *ProductService) UpdateProduct(product *model.Product) (*model.Product, error) {
	product, err := ps.ProductRepo.UpdateProduct(product)
	if err != nil {
		return nil, err
	}

	return product, nil
}

func (ps *ProductService) DeleteProduct(upc string) error {
	err := ps.ProductRepo.DeleteProduct(upc)
	if err != nil {
		return err
	}

	return nil
}
