module gitlab.com/adred/go-graphql-gateway-demo/product

go 1.18

require (
	github.com/99designs/gqlgen v0.16.0
	github.com/go-chi/chi/v5 v5.0.7
	github.com/gocql/gocql v0.0.0-20211222173705-d73e6b1002a7
	github.com/google/uuid v1.3.0
	github.com/gorilla/websocket v1.4.2
	github.com/jensneuse/abstractlogger v0.0.4
	github.com/vektah/gqlparser/v2 v2.3.1
	go.uber.org/atomic v1.9.0
	go.uber.org/zap v1.21.0
)

require (
	github.com/agnivade/levenshtein v1.1.0 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.1 // indirect
	github.com/golang/snappy v0.0.3 // indirect
	github.com/hailocab/go-hostpool v0.0.0-20160125115350-e80d13ce29ed // indirect
	github.com/hashicorp/golang-lru v0.5.0 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.1 // indirect
	github.com/logrusorgru/aurora/v3 v3.0.0 // indirect
	github.com/matryer/moq v0.2.3 // indirect
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/mitchellh/mapstructure v1.2.3 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/sirupsen/logrus v1.4.2 // indirect
	github.com/urfave/cli/v2 v2.3.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/mod v0.4.2 // indirect
	golang.org/x/sys v0.0.0-20210510120138-977fb7262007 // indirect
	golang.org/x/tools v0.1.5 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/inf.v0 v0.9.1 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
