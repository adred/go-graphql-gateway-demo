#!/bin/bash

if [ -f ./.env ]
then
  export $(cat ./.env | sed 's/#.*//g' | xargs)
fi

go build -o /tmp/product-service ./server.go

/tmp/product-service
