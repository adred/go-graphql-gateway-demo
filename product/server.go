package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/go-chi/chi/v5"
	"github.com/gocql/gocql"
	log "github.com/jensneuse/abstractlogger"
	"go.uber.org/zap"

	"gitlab.com/adred/go-graphql-gateway-demo/product/graph"
	"gitlab.com/adred/go-graphql-gateway-demo/product/graph/generated"
	"gitlab.com/adred/go-graphql-gateway-demo/product/repository"
	"gitlab.com/adred/go-graphql-gateway-demo/product/service"
)

const defaultPort = "4002"

func logger() log.Logger {
	logger, err := zap.NewDevelopmentConfig().Build()
	if err != nil {
		panic(err)
	}

	return log.NewZapLogger(logger, log.DebugLevel)
}

func startServer() {
	logger := logger()
	logger.Info("logger initialized")

	dbHost := os.Getenv("DB_HOST")
	dbKeySpace := os.Getenv("DB_KEY_SPACE")
	dbUsername := os.Getenv("DB_UNAME")
	dbPassword := os.Getenv("DB_PASS")
	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	c := gocql.NewCluster(dbHost)
	c.Keyspace = dbKeySpace
	c.Consistency = gocql.Quorum
	c.Authenticator = gocql.PasswordAuthenticator{Username: dbUsername, Password: dbPassword}
	session, err := c.CreateSession()
	if err != nil {
		panic(err)
	}

	ur := repository.NewProductRepository(session)
	us := service.NewProductService(ur)
	pur := repository.NewProductsByUserRepository(session)
	pus := service.NewProductsByUserService(pur)

	r := chi.NewRouter()

	graphqlHandler := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{
		ProductService:        us,
		ProductsByUserService: pus,
	}}))

	r.Post("/query", func(w http.ResponseWriter, r *http.Request) {
		graphqlHandler.ServeHTTP(w, r)
	})

	r.Get("/playground", func(w http.ResponseWriter, r *http.Request) {
		playground.Handler("GraphQL playground", "/query").ServeHTTP(w, r)
	})

	fmt.Printf("connect to http://<server_domain>:%s/ for GraphQL playground", port)
	logger.Fatal("failed listening", log.Error(http.ListenAndServe(":"+port, r)))
}

func main() {
	startServer()
}
