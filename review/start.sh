#!/bin/bash

if [ -f ./.env ]
then
  export $(cat ./.env | sed 's/#.*//g' | xargs)
fi

go build -o /tmp/review-service ./server.go

/tmp/review-service
