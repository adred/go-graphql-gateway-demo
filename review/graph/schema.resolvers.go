package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"github.com/google/uuid"
	"gitlab.com/adred/go-graphql-gateway-demo/review/graph/generated"
	"gitlab.com/adred/go-graphql-gateway-demo/review/graph/model"
)

func (r *mutationResolver) CreateReview(ctx context.Context, input model.CreateReview) (*model.Review, error) {
	rv, err := r.ReviewService.CreateReview(&model.Review{
		ID:      uuid.NewString(),
		Body:    input.Body,
		Author:  &model.User{ID: input.Author},
		Product: &model.Product{Upc: input.Product},
	})

	if err != nil {
		return &model.Review{}, err
	}

	return rv, nil
}

func (r *productResolver) Reviews(ctx context.Context, obj *model.Product) ([]*model.Review, error) {
	// Just for demo. Not efficient
	rs, err := r.ReviewService.GetAllReviews()
	if err != nil {
		return []*model.Review{}, err
	}

	var reviews []*model.Review
	for _, val := range rs {
		if val.Product.Upc == obj.Upc {
			reviews = append(reviews, val)
		}
	}

	return reviews, nil
}

func (r *queryResolver) GetAllReviews(ctx context.Context) ([]*model.Review, error) {
	// Just for demo. Not efficient
	rs, err := r.ReviewService.GetAllReviews()
	if err != nil {
		return []*model.Review{}, err
	}

	return rs, nil
}

func (r *userResolver) Reviews(ctx context.Context, obj *model.User) ([]*model.Review, error) {
	// Just for demo. Not efficient
	rs, err := r.ReviewService.GetAllReviews()
	if err != nil {
		return []*model.Review{}, err
	}

	var reviews []*model.Review
	for _, val := range rs {
		if val.Author.ID == obj.ID {
			reviews = append(reviews, val)
		}
	}

	return reviews, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Product returns generated.ProductResolver implementation.
func (r *Resolver) Product() generated.ProductResolver { return &productResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

// User returns generated.UserResolver implementation.
func (r *Resolver) User() generated.UserResolver { return &userResolver{r} }

type mutationResolver struct{ *Resolver }
type productResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
type userResolver struct{ *Resolver }
