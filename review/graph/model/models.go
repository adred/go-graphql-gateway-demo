package model

type Product struct {
	Upc string `json:"upc"`
}

func (Product) IsEntity() {}

type Review struct {
	ID      string
	Body    string
	Author  *User
	Product *Product
}

func (Review) IsEntity() {}

type User struct {
	ID string `json:"id"`
}

func (User) IsEntity() {}
