// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.
package graph

import (
	"gitlab.com/adred/go-graphql-gateway-demo/review/service"
)

type Resolver struct {
	ReviewService *service.ReviewService
}
