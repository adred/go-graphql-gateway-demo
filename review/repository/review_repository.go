package repository

import (
	"github.com/gocql/gocql"

	"gitlab.com/adred/go-graphql-gateway-demo/review/graph/model"
)

var (
	reviewTable = "review"

	createReviewQuery  = "INSERT INTO " + reviewTable + " (id, product, author, body) VALUES(?, ?, ?, ?)"
	getReviewQuery     = "SELECT * FROM " + reviewTable + " WHERE id=? LIMIT 1"
	getAllReviewsQuery = "SELECT * FROM " + reviewTable + ""
	updateReviewQuery  = "UPDATE " + reviewTable + " SET body = ? WHERE id=?"
	deleteReviewQuery  = "DELETE FROM " + reviewTable + " WHERE id=?"
)

type ReviewRepositoryInterface interface {
	CreateReview(*model.Review) (*model.Review, error)
	GetReview(id string) (*model.Review, error)
	GetAllReviews() ([]*model.Review, error)
	UpdateReview(review *model.Review) (*model.Review, error)
	DeleteReview(product string) error
}
type ReviewRepository struct {
	session *gocql.Session
}

func NewReviewRepository(session *gocql.Session) *ReviewRepository {
	return &ReviewRepository{
		session,
	}
}

var _ ReviewRepositoryInterface = &ReviewRepository{}

func (rr *ReviewRepository) CreateReview(review *model.Review) (*model.Review, error) {
	if err := rr.session.Query(createReviewQuery, review.ID, review.Product.Upc, review.Author.ID,
		review.Body).Exec(); err != nil {
		return &model.Review{}, err
	}
	return review, nil
}

func (rr *ReviewRepository) GetReview(id string) (*model.Review, error) {
	var _id gocql.UUID
	var product gocql.UUID
	var author gocql.UUID
	var body string

	err := rr.session.Query(getReviewQuery,
		id).Scan(&_id, &product, &author, &body)

	if err != nil {
		return &model.Review{}, err
	}

	return &model.Review{
		ID:      _id.String(),
		Author:  &model.User{ID: author.String()},
		Product: &model.Product{Upc: product.String()},
		Body:    body,
	}, nil
}

func (rr *ReviewRepository) GetAllReviews() ([]*model.Review, error) {
	var reviews []*model.Review
	t := map[string]interface{}{}

	iter := rr.session.Query(getAllReviewsQuery).Iter()
	for iter.MapScan(t) {
		reviews = append(reviews, &model.Review{
			ID:      t["id"].(gocql.UUID).String(),
			Author:  &model.User{ID: t["author"].(gocql.UUID).String()},
			Product: &model.Product{Upc: t["product"].(gocql.UUID).String()},
			Body:    t["body"].(string),
		})
		t = map[string]interface{}{}
	}

	return reviews, nil
}

func (rr *ReviewRepository) UpdateReview(review *model.Review) (*model.Review, error) {
	if err := rr.session.Query(updateReviewQuery, review.Body, review.Product.Upc).Exec(); err != nil {
		return &model.Review{}, err
	}

	return review, nil
}

func (rr *ReviewRepository) DeleteReview(product string) error {
	if err := rr.session.Query(deleteReviewQuery, product).Exec(); err != nil {
		return err
	}

	return nil
}
