package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/go-chi/chi/v5"
	"github.com/gocql/gocql"
	log "github.com/jensneuse/abstractlogger"
	"go.uber.org/zap"

	"gitlab.com/adred/go-graphql-gateway-demo/review/graph"
	"gitlab.com/adred/go-graphql-gateway-demo/review/graph/generated"
	"gitlab.com/adred/go-graphql-gateway-demo/review/repository"
	"gitlab.com/adred/go-graphql-gateway-demo/review/service"
)

const defaultPort = "4003"

func logger() log.Logger {
	logger, err := zap.NewDevelopmentConfig().Build()
	if err != nil {
		panic(err)
	}

	return log.NewZapLogger(logger, log.DebugLevel)
}

func startServer() {
	logger := logger()
	logger.Info("logger initialized")

	dbHost := os.Getenv("DB_HOST")
	dbKeySpace := os.Getenv("DB_KEY_SPACE")
	dbUsername := os.Getenv("DB_UNAME")
	dbPassword := os.Getenv("DB_PASS")
	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	c := gocql.NewCluster(dbHost)
	c.Keyspace = dbKeySpace
	c.Consistency = gocql.Quorum
	c.Authenticator = gocql.PasswordAuthenticator{Username: dbUsername, Password: dbPassword}
	session, err := c.CreateSession()
	if err != nil {
		panic(err)
	}

	rr := repository.NewReviewRepository(session)
	rs := service.NewReviewService(rr)

	r := chi.NewRouter()

	graphqlHandler := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{
		ReviewService: rs,
	}}))

	r.Post("/query", func(w http.ResponseWriter, r *http.Request) {
		graphqlHandler.ServeHTTP(w, r)
	})

	r.Get("/playground", func(w http.ResponseWriter, r *http.Request) {
		playground.Handler("GraphQL playground", "/query").ServeHTTP(w, r)
	})

	fmt.Printf("connect to http://<server_domain>:%s/ for GraphQL playground", port)
	logger.Fatal("failed listening", log.Error(http.ListenAndServe(":"+port, r)))
}

func main() {
	startServer()
}
