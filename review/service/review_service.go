package service

import (
	"gitlab.com/adred/go-graphql-gateway-demo/review/graph/model"
	"gitlab.com/adred/go-graphql-gateway-demo/review/repository"
)

type ReviewServiceInterface interface {
	CreateReview(*model.Review) (*model.Review, error)
	GetReview(id string) (*model.Review, error)
	GetAllReviews() ([]*model.Review, error)
	UpdateReview(review *model.Review) (*model.Review, error)
	DeleteReview(id string) error
}

type ReviewService struct {
	ReviewRepo *repository.ReviewRepository
}

func NewReviewService(tr *repository.ReviewRepository) *ReviewService {
	return &ReviewService{ReviewRepo: tr}
}

// ReviewService implements the model.ReviewInterface interface
var _ ReviewServiceInterface = &ReviewService{}

func (rs *ReviewService) CreateReview(review *model.Review) (*model.Review, error) {
	review, err := rs.ReviewRepo.CreateReview(review)
	if err != nil {
		return nil, err
	}

	return review, nil
}

func (rs *ReviewService) GetReview(id string) (*model.Review, error) {
	r, err := rs.ReviewRepo.GetReview(id)
	if err != nil {
		return nil, err
	}
	return r, nil
}

func (rs *ReviewService) GetAllReviews() ([]*model.Review, error) {
	reviews, err := rs.ReviewRepo.GetAllReviews()
	if err != nil {
		return nil, err
	}
	return reviews, nil
}

func (rs *ReviewService) UpdateReview(review *model.Review) (*model.Review, error) {
	review, err := rs.ReviewRepo.UpdateReview(review)
	if err != nil {
		return nil, err
	}

	return review, nil
}

func (rs *ReviewService) DeleteReview(id string) error {
	err := rs.ReviewRepo.DeleteReview(id)
	if err != nil {
		return err
	}

	return nil
}
